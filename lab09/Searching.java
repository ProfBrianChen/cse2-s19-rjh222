///////////////////
// Ricky Hart lab9
// this program will search arrays in different ways

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.util.Scanner; //imports scanner class

public class Searching{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //accepting scanner
    String junkWord; //initializes junkword
    boolean isString = false;//intializes string as false so it starts entering a number
    String user = "";//intiales string
    do{
        System.out.print("Do you want a linear or binary search? Please enter in lowercase letters: "); //prints words to ask for shape
        if(myScanner.hasNext()){ //checks if value is a value
          user = myScanner.next();//inserts into shape variable
            if(user.equals("linear") || user.equals("binary")){ //checks if input is equal to the strings we want
            isString = true;  //exits the loop
            }
        }
        else{
          System.out.println("Error. Make sure you enter insert or shorten in lowercase: ");
          junkWord = myScanner.next(); //clears belt
        }
      } while(!(isString));  //keeps in loop until we get the desired result
    
    if (user.equals("linear")){
    System.out.print("Enter an array size: ");
    while (myScanner.hasNextInt() == false){ //keeps in loop if they added wrong value
      junkWord = myScanner.next(); //cleans "belt"
      System.out.print("An error occured, make sure you enter a number: "); //tells them to re enter
    } 
    int size = myScanner.nextInt(); //gets size from user
    System.out.print("Enter a value to search for: ");
    while (myScanner.hasNextInt() == false){ //keeps in loop if they added wrong value
      junkWord = myScanner.next(); //cleans "belt"
      System.out.print("An error occured, make sure you enter a number: "); //tells them to re enter
    } 
    int search = myScanner.nextInt(); //gets size from user
    int[] array = new int[size]; //initailiizes array
    array = generateLin(array); //calls method
    System.out.println("The array is: " + Arrays.toString(array));
    int index = linSearch(array, search); //calls method
    System.out.println("Found at: " + index);
    }
    
    if (user.equals("binary")){
    System.out.print("Enter an array size: ");
    while (myScanner.hasNextInt() == false){ //keeps in loop if they added wrong value
      junkWord = myScanner.next(); //cleans "belt"
      System.out.print("An error occured, make sure you enter a number: "); //tells them to re enter
    } 
    int size = myScanner.nextInt(); //gets size from user
    System.out.print("Enter a value to search for: ");
    while (myScanner.hasNextInt() == false){ //keeps in loop if they added wrong value
      junkWord = myScanner.next(); //cleans "belt"
      System.out.print("An error occured, make sure you enter a number: "); //tells them to re enter
    } 
    int search = myScanner.nextInt(); //gets size from user
    int[] array = new int[size]; //intializes array
    array = generateBin(array); //calls method
    System.out.println("The array is: " + Arrays.toString(array));
    int index = binSearch(array, search); //calls method
    System.out.println("Found at: " + index);
    }
  }
  
  public static int[] generateLin(int[] a){
    Random r = new Random(); //accpets random
    int low = 0; //sets low
    int size = a.length;//finds size
    int high = size; //sets high
    for(int i = 0; i < size; i++){
      int result = r.nextInt(high - low) + low; //finds random number between 0 and size
      a[i] = result; //generates random numbers
      }
    return a;
  }
  
  public static int[] generateBin(int[] a){
    int size = a.length;
    for(int i = 0; i < size; i++){
      a[i] = i; //creates ascending values in index
    }
    return a;
  }
  
  public static int linSearch(int[] a, int search){
     int size = a.length;
     for(int i = 0; i < size; i++){ //checks every index for value
       if(a[i] == search){
         return i; //returns value if found at index
       }
     }
     return -1;
  }

  public static int binSearch(int[]a, int search){
    if(search > a.length){
      return -1; //returns -1 if value is bigger to prevent RE
    }
    int low = 0; //sets low
    int high = a.length; //sets high
    while(high >= low){ //keeps in loop until high > low
      int middle = (low+high)/2; //finds middle
      if(a[middle] == search){
        return middle; //returns index when found
      }
      if(a[middle] < search){ //changes low to 1 aboce middle
        low = middle + 1;
      }
      if(a[middle] > search){ //changes high to below middle
        high = middle -1;
      }
    }
    return -1;  
  }

}
