////////////////
///CSE2 HW02
// Ricky Hart 2/10/19
// This program will manipulate data stored in variables, run calculations, and printing the output
public class Arithmetic {
  public static void main (String args []) {
    int numPants = 3; // stores number of pants as variable
    double pantsPrice = 34.98; //stores the price of pants
    int numShirts = 2; //storoes number of sweatshirts
    double shirtPrice = 24.99; //stores price of sweatshirts
    int numBelts = 1; //stores number of belts
    double beltPrice = 33.99; //stores price of belts
    double paSalesTax = .06; //stores the tax rate
    double pantsCost = numPants * pantsPrice; //cost of pants you bought
    double shirtCost = numShirts * shirtPrice; //cost of shirts bought
    double beltCost = numBelts * beltPrice; // cost of belts
    double taxPants = pantsCost * paSalesTax; // sales tax for pants
    double taxShirts = shirtCost * paSalesTax; //sales tax for shirts
    double taxBelts = beltCost *paSalesTax; // sales tax for belts
    double costBefore = pantsCost + shirtCost + beltCost; //price before tax
    double totalTax = taxPants + taxShirts + taxBelts; //total sales tax
    double totalCost = costBefore + totalTax; //price of whole transaction
    double pantsCost1 = (int) (pantsCost * 100); //getting ready to convert to xx.xx form
    double pantsCost2 = pantsCost1/100; //converted to xx.xx form
    double shirtCost1 = (int) (shirtCost * 100); //getting ready to convert to xx.xx form
    double shirtCost2 = shirtCost1/100; //converted to xx.xx form
    double beltCost1 = (int) (beltCost * 100); //getting ready to convert to xx.xx form
    double beltCost2 = beltCost1/100; //converted to xx.xx form
    double pantsTax1 = (int) (taxPants * 100); //getting ready to convert to xx.xx form
    double pantsTax2 = pantsTax1/100; //converted to xx.xx form
    double shirtTax1 = (int) (taxShirts * 100); //getting ready to convert to xx.xx form
    double shirtTax2 = shirtTax1/100; //converted to xx.xx form
    double beltTax1 = (int) (taxBelts * 100); //getting ready to convert to xx.xx form
    double beltTax2 = beltTax1/100; //converted to xx.xx form
    double costBefore1 = (int) (costBefore * 100); //getting ready to convert to xx.xx form
    double costBefore2 = costBefore1/100; //converted to xx.xx form
    double totalTax1 = (int) (totalTax * 100); //getting ready to convert to xx.xx form
    double totalTax2 = totalTax1/100; //converted to xx.xx form
    double totalCost1 = (int) (totalCost * 100); //getting ready to convert to xx.xx form
    double totalCost2 = totalCost1/100; //converted to xx.xx form
    System.out.println("Cost of pants before tax $" + pantsCost2); //prints cost in xx.xx form
    System.out.println("Tax on Pants $" + pantsTax2); //prints tax in xx.xx form
    System.out.println("Cost of Sweatshirts before Tax $" + shirtCost2); //prints cost in xx.xx form
    System.out.println("Tax on Sweatshirts $" + shirtTax2); //prints tax in xx.xx form
    System.out.println("Cost of Belts before Tax $" + beltCost2); //prints cost in xx.xx form
    System.out.println("Tax on Belts $" + beltTax2); //prints tax in xx.xx form
    System.out.println("Cost of transaction before tax $" + costBefore2); //prints cost in xx.xx form
    System.out.println("Total of all the tax charged $" + totalTax2); //prints tax in xx.xx form
    System.out.println("Cost of transaction including tax $" + totalCost2); //prints cost in xx.xx form
    
    
    
  }
}