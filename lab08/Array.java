///////////////////
// Ricky Hart lab8
// this program will create na array and populate it with random integers

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.lang.Math; //imports math functions

public class Array{
  public static void main(String args []){
    Random r = new Random();
    int low = 50;
    int high = 100;
    int result = r.nextInt(high - low) + low;
    int [] array = new int[result];
    array = fillArray(array);
    Arrays.sort(array);
    int range = getRange(array);
    double mean = getMean(array);
    double stdDev = Math.sqrt(getstdDev(array, getMean(array)));
    System.out.println(Arrays.toString(array));
    System.out.println("The range is: " + range);
    System.out.println("The mean is: " + mean);
    System.out.println("The Standard Deviation is: " + stdDev);
  }
    
    public static int getRange(int[] a){
      int range = (a[a.length-1] - a[0]);
      return range;
    }
   
    public static double getMean(int[] a){ 
      double sum = 0;
      int size = a.length;
      for(int j = 0; j < size; j++){
        sum = sum + a[j];
      }
      double average = sum/size;
      return average;
    }
    
    public static double getstdDev(int[] a, double mean){ 
    double dev = 0;
    int size = a.length;
    for(int k = 0; k < size; k++){
      dev = dev + ((Math.pow((a[k] - mean),2)/(size - 1)));
    }
    return dev;
    }
    
    public static int[] fillArray(int[] a){
      for (int i = 0; i < a.length; i++){
        a[i] = (int)((Math.random()*100));  
      }
      return a;
    }
}


