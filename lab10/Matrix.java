///////////////////
// Ricky Hart lab10
// this program operate on matrices

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.util.Scanner; //imports scanner class

public class Matrix{
  public static int[][] increasingMatrix(int width, int height, boolean format){
    if (format == true){
      int[][] a = new int[width][];
      for(int k = 0; k < width; k++){
        a[k] = new int[height];
      }  
      int counter = 1;
      for(int i = 0; i < width; i++){
        for(int j =0; j < height; j++){
          a[i][j] = counter;
          counter++;
        }
      }
    return a;  
    }
    else{
      int[][] a = new int[width][height];
      int cellValue = 1;
      for(int rowIndex = 0 ; rowIndex < height ; rowIndex++) {
        for(int columnIndex = 0 ; columnIndex < width ; columnIndex++, cellValue++) {
            a[columnIndex][rowIndex] = cellValue;
          cellValue = cellValue + height - 1;
        }
      cellValue = rowIndex + 2;  
      }
    return a;
    }
  }
  
  public static void printMatrix(int[][] array, boolean format){
    if (format == true){
      for(int i = 0; i < array.length; i++){
        for(int j = 0; j<array[0].length;j++){
          System.out.print(array[i][j] + ", ");
        }
      System.out.println();  
      }
    System.out.println();  
    }
    if (format == false){
     for(int i = 0; i < array.length; i++){
        for(int j =0; j <array[0].length; j++){
          System.out.print(array[i][j] + ", ");
        }
      System.out.println();   
      }
    System.out.println();  
    }
    if (array == null){
      System.out.println("The array was empty!");
    }
  }
  
  public static int[][] translate(int[][]array){
    int counter = 1;
      for(int i = 0; i < array.length; i++){
        for(int j =0; j < array[i].length; j++){
          array[i][j] = counter;
          counter++;
        }
      }
      return array;
  }
  
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
    if (a.length == b.length && a[0].length == b[0].length){
        int[][] added = new int [a.length][a[0].length];
        for(int i = 0; i < a.length; i++){
          for (int j = 0; j < a[i].length; j++){
            added[i][j] = a[i][j] + b[i][j];
          }
        }
    return added;
    } 
    else {
      return null;
    }
  }
  
  public static void main(String args[]){
    int width1 = (int) (Math.random() * 10 + 1); //random number between 1 and 10
    int height1 = (int) (Math.random() * 10 + 1); //random number between 1 and 10
    int height2 = (int) (Math.random() * 10 + 1); //random number between 1 and 10
    int width2 = (int) (Math.random() * 10 + 1); //random number between 1 and 10
    int [][] A = new int[width1][height1];
    A = increasingMatrix(width1, height1, true);
    System.out.println("Matrix A:");
    printMatrix(A, true);
    int [][] B = new int[width1][height1];
    B = increasingMatrix(width1, height1, false);
    System.out.println("Matrix B:");
    printMatrix(B, false);
    int [][] newB = new int[width1][height1];
    newB = translate(B);
    System.out.println("Translated Matrix B:");
    printMatrix(newB, true);
    int [][] C = new int[width2][height2];
    C = increasingMatrix(width2, height2, true);
    System.out.println("Matrix C:");
    printMatrix(C, true);
    int[][] AB = new int[width1][height1];
    AB = addMatrix(A,true,B,true);
    int[][] AC = new int[width1][height1];
    AC = new int[width1][height1];
    AC = addMatrix(A,true,C,true);
    if (AB == null){
      System.out.println("Matrix A+B:");
      System.out.println("These arrays cannot be added!");
    }
    if (AB != null){
      System.out.println("Matrix A+B:");
      printMatrix(AB, true);
    }
    if (AC == null){
      System.out.println("Matrix A+C:");
      System.out.println("These arrays cannot be added!");
    }
    if (AC != null){
      System.out.println("Matrix A+C:");
      printMatrix(AC, true);
    }
  }
}