///////////////////////////
////HW5 Ricky Hart
//// This program will prompt user to ask for data regarding their course they are taking
import java.util.Scanner; //importing scanner class

public class Hw05 {
  public static void main (String args[]){
    int course = 0; //intializing variable
    String department = ""; //intializing variable
    int timesperweek = 0; //intializing variable
    String start = ""; //intializing variable
    String profName = ""; //intializing variable
    int numStudents = 0; //intializing variable
    String throwaway = "";//intializing variable 
    Scanner courseNum = new Scanner(System.in); //acepting scanner
    Scanner name = new Scanner(System.in); //acepting scanner
    Scanner days = new Scanner(System.in);//acepting scanner
    Scanner time = new Scanner(System.in);//acepting scanner
    Scanner prof = new Scanner(System.in);//acepting scanner
    Scanner numStuds = new Scanner(System.in);//acepting scanner
    
    System.out.print("Enter the Course Number: "); //getting course number
    while (courseNum.hasNextInt() == false){ //keeps in loop if they added wrong value
      throwaway = courseNum.next(); //cleans "belt"
      System.out.print("An error occured, make sure you entered the course number correctly: ");} //tells them to re enter
    course = courseNum.nextInt(); //gets value if correct value inputted
    
    System.out.print("Enter the name of the department: ");//getting department name
    while (name.hasNextLine() == false){//keeps in loop if they added wrong value
      throwaway = name.next();//cleans "belt"
      System.out.print("An error occured, make sure you entered the department name correctly: ");}//tells them to re enter
    department = name.nextLine();//gets value if correct value inputted
    
    System.out.print("Enter the number of times the class meets per week: "); //getting meetings per week
    while (days.hasNextInt() == false){//keeps in loop if they added wrong value
      throwaway = days.next();//cleans "belt"
      System.out.print("An error occured, make sure you entered the number of times you meet per week correctly: ");}//tells them to re enter
    timesperweek = days.nextInt();//gets value if correct value inputted
    
    System.out.print("Enter the time the class starts: "); //getting start time
    while (time.hasNextLine() == false){//keeps in loop if they added wrong value
      throwaway = time.next();//cleans "belt"
      System.out.print("An error occured, make sure you entered start time correctly: ");}//tells them to re enter
    start = time.nextLine();//gets value if correct value inputted
    
    System.out.print("Enter the professor's name: "); //getting prof name
    while (prof.hasNextLine() == false){//keeps in loop if they added wrong value
      throwaway = prof.next();//cleans "belt"
      System.out.print("An error occured, make sure you entered the course number correctly: ");}//tells them to re enter
    profName = prof.nextLine();//gets value if correct value inputted
    
    System.out.print("Enter the number of students: "); //getting number of students
    while (numStuds.hasNextInt() == false){//keeps in loop if they added wrong value
      throwaway = numStuds.next();//cleans "belt"
      System.out.print("An error occured, make sure you entered the number of students correctly correctly: ");}//tells them to re enter
    numStudents = numStuds.nextInt();//gets value if correct value inputted
    
    
  }
}