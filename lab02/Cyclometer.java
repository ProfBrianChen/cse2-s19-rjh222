//////////////
// CSE2 Lab 02 2/8/19
// Ricky Hart
// My program will measure the minutes for each trip, counts for each trip, miles of each trip, and distance
public class Cyclometer {
  //main method
  public static void main(String[] args) {
    // our input data
    int secsTrip1=480; // stores how long the first trip was
    int secsTrip2=3220; // stores how long trip 2 was
    int countsTrip1=1561; //stores how many counts made in trip 1
    int countsTrip2=9037; //stores how many counts in trip 2
    double wheelDiamter=27.0; //stores diameter of wheel
    double PI=3.141592; //stores pi as a value
    double feetPerMile=5280; // conversion for feet to mile
    double inchesPerFoot=12; // conversion for feet to inches
    double secondsPerMinute=60; // conversion between seconds and minutes
    double distanceTrip1, distanceTrip2, totalDistance; // creates variables for our distance
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+ " counts."); //print desired information
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+ " counts."); //print desired information
    distanceTrip1=countsTrip1*wheelDiamter*PI/inchesPerFoot/feetPerMile; // counting how far we moved during trip 1 using the circumfernce and number of revolutions in miles
    distanceTrip2=countsTrip2*wheelDiamter*PI/inchesPerFoot/feetPerMile; // counting how far we moved during trip 2 using the circumfernce and number of revolutions in miles
    totalDistance=distanceTrip2+distanceTrip1;// calculates total distacne over 2 trips
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints miles of trip
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints miles of trip
    System.out.println("The total distance was " +totalDistance+" miles.");//prints total miles
    
  }// end of main method
} // end of main class