///////////////////
// Ricky Hart lab8
// this program will create radnom arrays then ask the user if they want to run, insert or shorten then proceed from there

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.util.Scanner; //imports scanner class

public class ArrayGames{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //accepting scanner
    String junkWord; //initializes junkword
    boolean isString = false;//intializes string as false so it starts entering a number
    String user = "";//intiales string
      do{
        System.out.print("Do you want to insert or shorten? Please enter in lowercase letters: "); //prints words to ask for shape
        if(myScanner.hasNext()){ //checks if value is a value
          user = myScanner.next();//inserts into shape variable
            if(user.equals("insert") || user.equals("shorten")){ //checks if input is equal to the strings we want
            isString = true;  //exits the loop
            }
        }
        else{
          System.out.println("Error. Make sure you enter insert or shorten in lowercase: ");
          junkWord = myScanner.next(); //clears belt
        }
      } while(!(isString));  //keeps in loop until we get the desired result
    if(user.equals("insert")){
      Random r = new Random(); //accpets random
      int low = 10; //sets low
      int high = 20; //sets high
      int result = r.nextInt(high - low) + low; //finds random number between 10-20
      int [] array1 = new int[result]; //declares one array
      int result2 = r.nextInt(high - low) + low;//finds random number between 10-20
      int [] array2 = new int[result2]; //declares second array
      array1 = generate(array1); //calls method to put numbers in array
      array2 = generate(array2); //calls method to put numbers in array
      int [] inserted = new int[((array1.length) + (array2.length))]; //declares new array of length of both added
      inserted = insert(array1, array2); //calls method
      System.out.println("Input 1: " + Arrays.toString(array1)); //prints input 1
      System.out.println("Input 2: " + Arrays.toString(array2)); //prints input 2
      System.out.println("Output: " + Arrays.toString(inserted)); //prints output
    }
    if(user.equals("shorten")){
      Random r = new Random(); //accpets random
      int low = 10; //sets low
      int high = 20; //sets high
      int result = r.nextInt(high - low) + low; //finds random number between 10-20
      int [] array = new int[result]; //declares one array
      array = generate(array); //calls method to put numbers in array
      System.out.println("Input 1: " + Arrays.toString(array));
      System.out.print("Enter a number: ");
      while (myScanner.hasNextInt() == false){ //keeps in loop if they added wrong value
      junkWord = myScanner.next(); //cleans "belt"
      System.out.print("An error occured, make sure you enter a number: ");} //tells them to re enter
      int length = myScanner.nextInt(); //gets length from user
      System.out.println("Input 2: " + length);
      int[] shortened = shorten(array, length);
      System.out.println("Output: " + Arrays.toString(shortened));
    }
  }
  
  public static int[] insert(int[] a, int[] b){
    Random r = new Random(); // accpets random class;
    int low = 0; //sets lower bound
    int high = a.length; //sets upper bound
    int result = r.nextInt(high-low) + low; //finds random number on first array
    int asize = a.length; //finds length of a
    int bsize = b.length; //finds length of b
    int bindex = 0; //gets index for b to use in loops
    int aindex = result; //gets index for a after cutting it
    int[] inserted = new int[(asize + bsize)]; //creates new array of size a+b
    for(int i = 0; i < (result); i++){
      inserted[i] = a[i]; //value of inserted at i equals the value of a at i
    }
    for(int j = result; j < (bsize + result); j++){
      inserted[j] = b[bindex]; //value of inserted = value of b at bindex
      bindex = bindex + 1; //increment
    }
    for(int k = (result + bsize); k < (bsize + asize); k++){
      inserted[k] = a[aindex]; //value inserted at k = value of a at aindex
      aindex = aindex + 1; //increment
    }
    return inserted; 
    
  }
  
  public static int[] generate(int[] a){
    for (int i = 0; i < a.length; i++){
        a[i] = (int)((Math.random()*100));  //fills array with random values
      }
    return a;
  }
  
  public static int[] shorten(int[] a, int num){
        if(num >= a.length){
          int[] shortened = new int[a.length];//creates an array based on normal length
          for(int i = 0; i < a.length; i++){
            shortened[i] = a[i]; //fills new array with values from original
          }
        return shortened;
        }
        else{
          int[] shortened = new int[num];//creates an array with user value
          for(int i = 0; i < num; i++){
            shortened[i] = a[i]; //fills array with values from original but stops at the number
          }
        return shortened;  
        }
     
  }
  
}