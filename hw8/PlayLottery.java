///////////////////
// Ricky Hart hw08
// this program will play the lottery for the user

import java.util.Scanner; //imports scanner
import java.util.Arrays; //imports array stuff
import java.util.Random; // allows us to usee random
import java.util.ArrayList; //imports arraylist

public class PlayLottery{
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //accepts scanner
    int[] user = new int [5]; //creating array for values entered
    for (int i = 0; i < 5; i++){
      System.out.print("Enter the number you want in the lottery: ");
      user[i] = myScanner.nextInt(); //getting all of the numbers entered
    }
    int [] nums = new int[5]; //creating an array for the random numbers
    Random randomgenerator = new Random(); //accepts random numbers
    ArrayList<Integer> used = new ArrayList<Integer>(); //creates a dynamic size of an array
    for (int i = 0; i < 5; i ++){
      int newRandom; //intializes new number
      do{
        newRandom = randomgenerator.nextInt(60); //gets random numbers from 0-59
      } while (used.contains(newRandom)); //condition for do while
      nums[i] = newRandom; //adds random number to array
      used.add(newRandom); //adds to array to check if they are the same
    }
    System.out.println("The numbers you entered were: " + Arrays.toString(user)); //print user values
    System.out.println("The winning numbers were: " + Arrays.toString(nums)); //print random values
    boolean didWin = userWins(user, nums); //calls method
    if (didWin == true){
      System.out.println("You win!"); //print you win if true
    }
    else {
      System.out.println("You lose."); //print you lose if false
    }
    }
  
  
  public static boolean userWins (int[] user, int[] winning){
    for(int i = 0; i < 5; i++){
      if (user[i] == winning[i]){ //checks if they are equal
        return true; //returns true to main method
      }
    }
    return false; //returns false to main method if if statement not satisfied
  }  
}