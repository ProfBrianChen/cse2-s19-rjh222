///////////////////
// Ricky Hart hw08
// this program will seperate half of the alphabet from an inout

import java.util.Scanner; //imports scanner
import java.util.Arrays; //imports array stuff

public class Letters{
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in); //accepts scanner
    System.out.print("Enter a string of characters: "); //prompts user to inout string
    String input = myScanner.next(); //getsa string from user
    char[] random = input.toCharArray(); //converts to a char array
    System.out.println("The random array: " + Arrays.toString(random)); //prints random array
    getAtoM(random); //calls method
    getNtoZ(random); //calls method
    
    
  }
   public static void getAtoM (char[] list){
      int z = list.length; //finds length
      int ucounter = 0; //counter 
      int lcounter = 0;   //counter
      for(int y = 0; y < z; y++){
          if(list[y] == 'A' || list[y] == 'B' || list[y] == 'C' || list[y] == 'D' || list[y] == 'E' || list[y] == 'F' || list[y] == 'G' || list[y] == 'H' || list[y] == 'I' || list[y] == 'J' || list[y] == 'K' || list[y] == 'L' || list[y] == 'M' || list[y] == 'a' || list[y] == 'b' || list[y] == 'c' || list[y] == 'd' || list[y] == 'e' || list[y] == 'f' || list[y] == 'g' || list[y] == 'h' || list[y] == 'i' || list[y] == 'j' || list[y] == 'k' || list[y] == 'l' || list[y] == 'm'){
            ucounter = ucounter + 1; //counts all of the a to m upper and lowercase on inputted string
          }
      }
     char [] atom = new char [ucounter]; //creates char array w length of counter above
     for(int y = 0; y < z; y++){
         if(list[y] == 'A' || list[y] == 'B' || list[y] == 'C' || list[y] == 'D' || list[y] == 'E' || list[y] == 'F' || list[y] == 'G' || list[y] == 'H' || list[y] == 'I' || list[y] == 'J' || list[y] == 'K' || list[y] == 'L' || list[y] == 'M' || list[y] == 'a' || list[y] == 'b' || list[y] == 'c' || list[y] == 'd' || list[y] == 'e' || list[y] == 'f' || list[y] == 'g' || list[y] == 'h' || list[y] == 'i' || list[y] == 'j' || list[y] == 'k' || list[y] == 'l' || list[y] == 'm'){
           char u = list[y]; //gets character at that point
           atom[lcounter] = u; //puts character into an array
           lcounter = lcounter + 1; //increment counter
         }
     }                          
     System.out.println("The A to M characters: " + Arrays.toString(atom)); //print array
   }
     
    public static void getNtoZ(char[] list){
      int z = list.length; //finds length
      int ucounter = 0; //counter
      int lcounter = 0; //counter
      for(int y = 0; y < z; y++){
          if(list[y] == 'N' || list[y] == 'O' || list[y] == 'P' || list[y] == 'Q' || list[y] == 'R' || list[y] == 'S' || list[y] == 'T' || list[y] == 'U' || list[y] == 'V' || list[y] == 'W' || list[y] == 'X' || list[y] == 'Y' || list[y] == 'Z' || list[y] == 'n' || list[y] == 'o' || list[y] == 'p' || list[y] == 'q' || list[y] == 'r' || list[y] == 's' || list[y] == 't' || list[y] == 'u' || list[y] == 'v' || list[y] == 'w' || list[y] == 'x' || list[y] == 'y' || list[y] == 'z'){
            ucounter = ucounter + 1; //counts all of the n to z upper and lowercase on inputted string
          }
      }
     char [] ntoz = new char [ucounter];
     for(int y = 0; y < z; y++){
         if(list[y] == 'N' || list[y] == 'O' || list[y] == 'P' || list[y] == 'Q' || list[y] == 'R' || list[y] == 'S' || list[y] == 'T' || list[y] == 'U' || list[y] == 'V' || list[y] == 'W' || list[y] == 'X' || list[y] == 'Y' || list[y] == 'Z' || list[y] == 'n' || list[y] == 'o' || list[y] == 'p' || list[y] == 'q' || list[y] == 'r' || list[y] == 's' || list[y] == 't' || list[y] == 'u' || list[y] == 'v' || list[y] == 'w' || list[y] == 'x' || list[y] == 'y' || list[y] == 'z'){
           char u = list[y];//gets character at that point
           ntoz[lcounter] = u;//puts character into an array
           lcounter = lcounter + 1;//increment counter
         }
     }                          
    System.out.println("The N to Z characters: " + Arrays.toString(ntoz)); //print array
  
  
    }
}