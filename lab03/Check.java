// CSE2 lab03
//this program will use the scanner class to find the original cost of the bill, find the percentage they want to pay as tip, and how they want to split the bill to find out the price everyone needs to pay
import java.util.Scanner;

public class Check{
  //main method for java changed to check because cookies will not allow program to run
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //ready to accpet input
    System.out.print("Enter original cost of the bill in xx.xx form: "); // gets input from the user regarding bill
    double checkCost = myScanner.nextDouble(); //calls the nextlny() method
    System.out.print("Enter value of tip you wish to pay as a whole number: "); // gets input from the user regarding tip
    double tipPercent = myScanner.nextDouble();
    double tipDecimal = tipPercent/100; //converts percent tip to decimal tip
    System.out.print("Enter the number of people who went out to dinner: "); //gets the int of people who went out
    int numPeople = myScanner.nextInt();
    double totalCost;
    double costPerPerson;
    int dollars; //stores values of integers
    int dimes; //stores digits in the 10ths place
    int pennies; //stores digits in 100ths place
    double tip = checkCost * tipDecimal; //calculates the cost of the tip
    totalCost = tip + checkCost; //calculats value of entire bill
    costPerPerson = totalCost / numPeople; //divides total cost by the number of people
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson *100) % 10;
    System.out.println("Each person in the group pays $" + dollars + '.' + dimes + pennies); //prints final values
  } //end of the main method
} //end of the class 