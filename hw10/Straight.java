///////////////////
// Ricky Hart hw10
// this program will see if you have a straight
//I did not understand the instructions for this assignment. I have no idea where you wanted that value of k to come from
//so I just did it an easy way and it works 

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.util.Scanner; //imports scanner class
import java.util.Collections; //imports so i can shuffle

public class Straight{
  public static void main (String args[]){
    int[] shuffledDeck = new int[52]; //allocates array
    shuffledDeck = shuffle(shuffledDeck); //calls methid
    int[] hand = new int[5]; //allocates array
    hand = draw(shuffledDeck); //calls methid
    straight(hand);//calls method
  }
  
  public static int[] shuffle(int[] a){
    Random r = new Random(); //accpets random
    for (int i = 0; i < 52; i++){
      a[i] = i; //gets numbers 0-51
    }
    for (int j = 0; j < 52; j++){ //shuffling
      int randomPosition = r.nextInt(52); //finds postion
      int temp = a[j]; //holds the value
      a[randomPosition] = temp; //puts the value in random position
    }
    return a; 
  }
  
  public static void straight(int[] a){
    Arrays.sort(a); //sorts the array. it is easier than linear search because i didnt get the instructions
    if(a[1] == a[0] + 1 && a[2] == a[1] + 1 && a[3] == a[2] + 1 && a[4] == a[3] + 1){ //checks each part of the sorted array
      System.out.println("You have a Straight!");
    }
    else{
      System.out.println("You do not have a straight");
    }  
  }
  
  public static int[] draw(int[] a){
    int [] hand = new int[5];
    for (int i = 0; i < 5; i++){
      hand[i] = a[i];
    }
    return hand;
  }
}