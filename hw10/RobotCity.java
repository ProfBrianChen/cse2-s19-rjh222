///////////////////
// Ricky Hart hw10
// this program will create a city then destroy it
//cant do it

import java.util.Arrays; //imports array stuff
import java.util.Random; //imports random number
import java.util.Scanner; //imports scanner class

public class RobotCity{
  public static void main(String args[]){
    Random r = new Random(); //accpets random
    int low1 = 10; //sets low
    int high1 = 15; //sets high
    int etow = r.nextInt(high1 - low1) + low1; //finds random number between 10-15
    int low2 = 10; //sets low
    int high2 = 15; //sets high
    int ntos = r.nextInt(high2 - low2) + low2; //finds random number between 10-15
    int [][] city = new int[etow][ntos]; //alloactes array
    city = buildCity(etow, ntos); //calls method
    display(city); //calls mehotd
    int robots = (int) (Math.random() * 10 + 1); //gets number of robots 1-10
    int[][] attack = new int[etow][ntos]; //alloactes array
    attack = invade(city, robots, etow, ntos); //calls method
    int attacks = 0; //sets counter
    int[][] newCity = new int[etow][ntos];
    while (attacks < 5){
      newCity = update(attack); //calls method
      display(newCity); //call mehtod
      attacks ++; //increment
    }  
  }
  
  public static int[][] buildCity(int east, int north){
    Random r = new Random(); //accpets random
    int [][] a = new int [east][north]; //allocates array w size
    int low3 = 100; //sets low
    int high3 = 999; //sets high
    for (int i = 0; i < east; i++){ //loops thorugh first dimension
      for(int j = 0; j < north; j++){ ////loops thorugh second dimension
        int pop = r.nextInt(high3 - low3) + low3; //finds random number between 100-999
        a[i][j] = pop; //puts random number in array
      }
    }
    return a;    
  }
  
  public static void display(int[][] a){
    for(int row = 0; row < a.length; row++){
      for(int col = 0; col < a[0].length; col++){
          System.out.printf("%4d ", a[row][col]);
      }
      System.out.print("\n");
    }
    System.out.println();
  }
  
  public static int[][] invade(int[][] a, int k ,int east, int north){
    Random r = new Random(); //accpets random
    int robotsLeft = k;
    while (robotsLeft > 0){
      int low = 0; ////lower limit
      int high = east;//upper limit
      int etow = r.nextInt(high - low) + low;//gets random number in block
      int low1 = 0;//lower limit
      int high1 = north; //upper limit
      int ntos = r.nextInt(high1 - low1) + low1; //gets random number in block
      a[etow][ntos] = a[etow][ntos] * -1; //makes value negtaive
      robotsLeft = robotsLeft - 1;
    }
    return a;
    
    
  }
  
  public static int[][] update(int[][] a){
    int east = a.length; //finds length in east to west
    int north = a[0].length; //finds length of rectangluar array in 2nd dimension
    for (int i = 0; i < east; i++){
      for (int j = 0; i < north; i++){
        if (a[i][j] < 0){
          if (i == east - 1){
            a[i][j] = a[i][j]; //robots go off            
          }
          else{
            if(a[(i+1)][j] < 0){
            a[(i+1)][j] = a[(i+1)][j];//keeps block dsetsroyed
            }
            else{
              (a[(i+1)][j]) = (a[(i+1)][j]) * -1;//destroys new block
            }  
          }  
        }
      }
    }
    return a;
  }
  
}