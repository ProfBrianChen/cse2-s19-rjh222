////////////////
//CSE2 HW03 Program 2
// This program will find the volume of a box given the dimensions

import java.util.Scanner; //imports scanner so we can accpet input

public class BoxVolume{
  public static void main (String args[]) {
    Scanner inputDim = new Scanner (System.in); //declaring instance of scanner
    System.out.print("Enter the vaule for width: ");//gets user input for width
    double valWidth = inputDim.nextDouble();//accepts value for width
    System.out.print("Enter the vaule for length: ");//gets user input for length
    double valLength = inputDim.nextDouble();//accepts value for length
    System.out.print("Enter the vaule for height: ");//gets user input for height
    double valHeight = inputDim.nextDouble();//accepts value for height
    double Volume = valHeight * valWidth * valLength; //calculates volume
    System.out.println("The volume of the inside the box is: " + Volume); //prints volume value
        
  }
}