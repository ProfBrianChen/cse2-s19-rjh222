////////////////
//CSE2 HW03 Program 1
// This program will convert meters to inches

import java.util.Scanner; //imported the scanner so we can obtain input from user

public class Convert{
  public static void main (String args[]) {
    Scanner inputMeters = new Scanner ( System.in ); //creating variable for input
    System.out.print("Enter the number of meters in xx.xx form: "); //asking for user input
    double numMeters = inputMeters.nextDouble(); //accepts the inputed variable 
    double numInches = numMeters / .0254; //converts meters to inches
    System.out.println(numMeters + " meters is " + numInches + " inches."); //prints conversion
    
  }
}