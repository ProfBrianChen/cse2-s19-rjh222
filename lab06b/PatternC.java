///////////////////////////
////Lab 06 Ricky Hart
//// This program will create a pattern with loops
import java.util.Scanner; //importing scanner class

public class PatternC {
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //accepting scanner
    int length; //initalizes length
    String junkWord; //itializes junkword
    int a = 1;//initalizes counter
    int b = 0;//initalizes counter
    int c = 0;//initalizes counter
    
    System.out.print("Enter the length as a positive integer 1-10: "); //asking for length
    while (myScanner.hasNextInt() == false){ //creates loop to get rid of invalid numbers
    junkWord = myScanner.next(); //clears belt
    System.out.print("Error: Make sure you input a positive integer. Enter Again: ");} //prints error message
    length = myScanner.nextInt(); //finds length
    while (length < 0){//gets rid of negative lengths
    System.out.print("Error: Make sure you input a number between 1 and 10. Enter Again: ");//prints error message
    length = myScanner.nextInt();} //finds length
    while (length > 10){//gets rid of above 10
    System.out.print("Error: Make sure you input a number between 1 and 10. Enter Again: ");//prints error message
    length = myScanner.nextInt();} //finds length
    
    for (a = 1; a <= length; a++) { //loop for length
      for (b = length; b >= a; b--){ //loop for spaces
        System.out.print(" ");
      }
      for (c = a; c >= 1; c--){ //loop for numbers in reverse
        System.out.print("" + c);
      }
    System.out.println();//extra line
    }
  }
}