///////////////////////////
////Lab 06 Ricky Hart
//// This program will create a pattern with loops
import java.util.Scanner; //importing scanner class

public class PatternD {
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //accepting scanner
    int length; //initalizes length
    String junkWord; //itializes junkword
    int a = 1;//initalizes counter
    int b = 0;//initalizes counter
    System.out.print("Enter the length as a positive integer 1-10: "); //asking for length
    while (myScanner.hasNextInt() == false){ //creates loop to get rid of invalid numbers
    junkWord = myScanner.next(); //clears belt
    System.out.print("Error: Make sure you input a positive integer. Enter Again: ");} //prints error message
    length = myScanner.nextInt(); //finds length
    while (length < 0){ //gets rid of negative lengths
    System.out.print("Error: Make sure you input a number between 1 and 10. Enter Again: ");//prints error message
    length = myScanner.nextInt();} //finds length
    while (length > 10){//gets rid of above 10
    System.out.print("Error: Make sure you input a number between 1 and 10. Enter Again: ");//prints error message
    length = myScanner.nextInt();} //finds length
    
    for (a = length; a >= 1; a--) { //loop for length
      for (b = a; b >= 1; b--){ //loop for numbers
        System.out.print(b + " ");//prints out numbers
      }
    System.out.println();//extra line
    }
  }
}