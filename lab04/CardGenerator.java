/////////////////
/// CSE2 Lab04
/// this program will randomly pick a card out of 52

public class CardGenerator {
  public static void main (String args[]) {
    int Num = (int)(Math.random()*12)+2; // randomly generates a number 2-13
    String CardNum;
    switch (Num) { // we will use the switch statement to assign the random number to an actual card number
      case 1: CardNum = "2";
        break;
      case 2: CardNum = "3";
        break;
      case 3: CardNum = "4";
        break;
      case 4: CardNum = "5";
        break;
      case 5: CardNum = "6";
        break;
      case 6: CardNum = "7";
        break;  
      case 7: CardNum = "8";
        break;
      case 8: CardNum = "9";
        break;
      case 9: CardNum = "10";
        break;
      case 10: CardNum = "Jack";
        break;
      case 11: CardNum = "Queen";
        break;
      case 12: CardNum = "King";
        break;
      case 13: CardNum = "Ace";
        break;
      default: CardNum = "Invalid Card";
        break;
    }
     String suitType = "";
     int suitNum = (int)(Math.random()*4+1);
      if (suitNum == 1) {
        suitType = "Spades"; //assigns the random value to spades
      }
      if (suitNum == 2) {
        suitType = "Diamonds"; // assigns random value to diamonds
      }
      if (suitNum == 3) {
        suitType = "Clubs"; // assigns random value to clubs
      }
      if (suitNum == 4) {
        suitType = "Hearts"; // assigns random value to hearts 
      }
    System.out.println("Your card is the " + CardNum + " of " + suitType);
  }
}