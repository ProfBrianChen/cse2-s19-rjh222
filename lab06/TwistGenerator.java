///////////////////////////
////HW5 Ricky Hart
//// This program will create a pattern with loops
import java.util.Scanner; //importing scanner class

public class TwistGenerator {
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //accepting scanner
    int length = 0; //initalizes length
    String junkWord; //itializes junkword
    int i = 0;//initalizes counter
    System.out.print("Enter the length as a positive integer: "); //asking for length
    while (myScanner.hasNextInt() == false){ //creates loop to get rid of invalid numbers
      junkWord = myScanner.next(); //clears belt
      System.out.print("Error: Make sure you input a positive integer. Enter Again: ");} //prints error message
      length = myScanner.nextInt(); //finds length
    if (length < 1) { //asks if length is positive
      System.out.print("Error: Make sure you input a positive integer. Enter Again: "); //prints error code
      length = myScanner.nextInt();} //gets new value for length if its negative
    
    for (i = 0; i < length; i++) { //creates loop for top row
      if (i % 3 == 0) { //stops at 3 coloumns
        System.out.print("\\");} //prints
      else if (i % 3 == 1){ //stops at 1 column
        System.out.print(" ");} //prints
      else{ //stops at 2 coloumns
        System.out.print("/");} //prints
    }
    System.out.println(""); //prints space
    
    for (i =0; i < length; i++) { //creates loop for middle row
      if (i % 3 == 1){ //stops at 1 column
        System.out.print("X");} //prints
      else{ //stops at either 2 or 3 
        System.out.print(" ");} //prints
    }
    System.out.println(""); //prints space
    
    for (i =0; i < length; i++) { //creates loop for bottom row
      if (i % 3 == 0){ //stop at 3 coloumns
        System.out.print("/");} //prints
      else if (i % 3 == 1){ //stops at 1 column
        System.out.print(" ");} //prints
      else{ //stops at 2 coloumns
        System.out.print("\\");} //prints
    }
    System.out.println("");//prints space
  }
}