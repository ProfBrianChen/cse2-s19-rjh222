/////////////////
/// CSE2 HW04
/// this program will draw 5 cards and see if they have pairs or not.
public class PokerHandCheck {
  public static void main (String args[]) {
    int NumCards = 0; //intializing all the if statements
    String CardNum = "";//allows us to compare the values
    String CardNum1 = "";//allows us to compare the values
    String CardNum2 = "";//allows us to compare the values
    String CardNum3 = "";//allows us to compare the values
    String CardNum4 = ""; //allows us to compare the values
    String suitType = "";//allows us to print the suit type
    String suitType1 = "";//allows us to print the suit type
    String suitType2 = "";//allows us to print the suit type
    String suitType3 = "";//allows us to print the suit type
    String suitType4 = "";//allows us to print the suit type
    if (NumCards == 0) {
    int Num = (int)(Math.random()*12)+2; // randomly generates a number 2-13
      switch (Num) { // we will use the switch statement to assign the random number to an actual card number
        case 1: CardNum = "2";
          break;
        case 2: CardNum = "3";
          break;
        case 3: CardNum = "4";
          break;
        case 4: CardNum = "5";
          break;
        case 5: CardNum = "6";
          break;
        case 6: CardNum = "7";
          break;  
        case 7: CardNum = "8";
          break;
        case 8: CardNum = "9";
          break;
        case 9: CardNum = "10";
          break;
        case 10: CardNum = "Jack";
          break;
        case 11: CardNum = "Queen";
          break;
        case 12: CardNum = "King";
          break;
        case 13: CardNum = "Ace";
          break;
        default: CardNum = "Invalid Card";
          break;
      }
       int suitNum = (int)(Math.random()*4+1);
        if (suitNum == 1) {
          suitType = "Spades"; //assigns the random value to spades
        }
        if (suitNum == 2) {
          suitType = "Diamonds"; // assigns random value to diamonds
        }
        if (suitNum == 3) {
          suitType = "Clubs"; // assigns random value to clubs
        }
        if (suitNum == 4) {
          suitType = "Hearts"; // assigns random value to hearts 
        }
      NumCards = NumCards + 1; //making maiking a "loop" while not making a loop, but allowing program to know to draw another card.
          }
     if (NumCards == 1) {
      int Num1 = (int)(Math.random()*12)+2; // randomly generates a number 2-13
      switch (Num1) { // we will use the switch statement to assign the random number to an actual card number
        case 1: CardNum1 = "2";
          break;
        case 2: CardNum1 = "3";
          break;
        case 3: CardNum1 = "4";
          break;
        case 4: CardNum1 = "5";
          break;
        case 5: CardNum1 = "6";
          break;
        case 6: CardNum1 = "7";
          break;  
        case 7: CardNum1 = "8";
          break;
        case 8: CardNum1 = "9";
          break;
        case 9: CardNum1 = "10";
          break;
        case 10: CardNum1 = "Jack";
          break;
        case 11: CardNum1 = "Queen";
          break;
        case 12: CardNum1 = "King";
          break;
        case 13: CardNum1 = "Ace";
          break;
        default: CardNum1 = "Invalid Card";
          break;
      }
       int suitNum1 = (int)(Math.random()*4+1);
        if (suitNum1 == 1) {
          suitType1 = "Spades"; //assigns the random value to spades
        }
        if (suitNum1 == 2) {
          suitType1 = "Diamonds"; // assigns random value to diamonds
        }
        if (suitNum1 == 3) {
          suitType1 = "Clubs"; // assigns random value to clubs
        }
        if (suitNum1 == 4) {
          suitType1 = "Hearts"; // assigns random value to hearts 
        }
      NumCards = NumCards + 1; //making a "loop" while not making a loop, but allowing program to know to draw another card.
    }
        if (NumCards == 2) {
      int Num2 = (int)(Math.random()*12)+2; // randomly generates a number 2-13
      switch (Num2) { // we will use the switch statement to assign the random number to an actual card number
        case 1: CardNum2 = "2";
          break;
        case 2: CardNum2 = "3";
          break;
        case 3: CardNum2 = "4";
          break;
        case 4: CardNum2 = "5";
          break;
        case 5: CardNum2 = "6";
          break;
        case 6: CardNum2 = "7";
          break;  
        case 7: CardNum2 = "8";
          break;
        case 8: CardNum2 = "9";
          break;
        case 9: CardNum2 = "10";
          break;
        case 10: CardNum2 = "Jack";
          break;
        case 11: CardNum2 = "Queen";
          break;
        case 12: CardNum2 = "King";
          break;
        case 13: CardNum2 = "Ace";
          break;
        default: CardNum2 = "Invalid Card";
          break;
      }
       int suitNum2 = (int)(Math.random()*4+1);
        if (suitNum2 == 1) {
          suitType2 = "Spades"; //assigns the random value to spades
        }
        if (suitNum2 == 2) {
          suitType2 = "Diamonds"; // assigns random value to diamonds
        }
        if (suitNum2 == 3) {
          suitType2 = "Clubs"; // assigns random value to clubs
        }
        if (suitNum2 == 4) {
          suitType2 = "Hearts"; // assigns random value to hearts 
        }
      NumCards = NumCards + 1; //making maiking a "loop" while not making a loop, but allowing program to know to draw another card.
          }
     if (NumCards == 3) {
      int Num3 = (int)(Math.random()*12)+2; // randomly generates a number 2-13
      switch (Num3) { // we will use the switch statement to assign the random number to an actual card number
        case 1: CardNum3 = "2";
          break;
        case 2: CardNum3 = "3";
          break;
        case 3: CardNum3 = "4";
          break;
        case 4: CardNum3 = "5";
          break;
        case 5: CardNum3 = "6";
          break;
        case 6: CardNum3 = "7";
          break;  
        case 7: CardNum3 = "8";
          break;
        case 8: CardNum3 = "9";
          break;
        case 9: CardNum3 = "10";
          break;
        case 10: CardNum3 = "Jack";
          break;
        case 11: CardNum3 = "Queen";
          break;
        case 12: CardNum3 = "King";
          break;
        case 13: CardNum3 = "Ace";
          break;
        default: CardNum3 = "Invalid Card";
          break;
      }
       int suitNum3 = (int)(Math.random()*4+1);
        if (suitNum3 == 1) {
          suitType3 = "Spades"; //assigns the random value to spades
        }
        if (suitNum3 == 2) {
          suitType3 = "Diamonds"; // assigns random value to diamonds
        }
        if (suitNum3 == 3) {
          suitType3 = "Clubs"; // assigns random value to clubs
        }
        if (suitNum3 == 4) {
          suitType3 = "Hearts"; // assigns random value to hearts 
        }
      NumCards = NumCards + 1; //making a "loop" while not making a loop, but allowing program to know to draw another card.
    }
         if (NumCards == 4) {
      int Num4 = (int)(Math.random()*12)+2; // randomly generates a number 2-13
      switch (Num4) { // we will use the switch statement to assign the random number to an actual card number
        case 1: CardNum4 = "2";
          break;
        case 2: CardNum4 = "3";
          break;
        case 3: CardNum4 = "4";
          break;
        case 4: CardNum4 = "5";
          break;
        case 5: CardNum4 = "6";
          break;
        case 6: CardNum4 = "7";
          break;  
        case 7: CardNum4 = "8";
          break;
        case 8: CardNum4 = "9";
          break;
        case 9: CardNum4 = "10";
          break;
        case 10: CardNum4 = "Jack";
          break;
        case 11: CardNum4 = "Queen";
          break;
        case 12: CardNum4 = "King";
          break;
        case 13: CardNum4 = "Ace";
          break;
        default: CardNum4 = "Invalid Card";
          break;
      }
       int suitNum4 = (int)(Math.random()*4+1);
        if (suitNum4 == 1) {
          suitType4 = "Spades"; //assigns the random value to spades
        }
        if (suitNum4 == 2) {
          suitType4 = "Diamonds"; // assigns random value to diamonds
        }
        if (suitNum4 == 3) {
          suitType4 = "Clubs"; // assigns random value to clubs
        }
        if (suitNum4 == 4) {
          suitType4 = "Hearts"; // assigns random value to hearts 
        }
      NumCards = 0; //making a "loop" while not making a loop, but allowing program to know to draw another card.
    }
    System.out.println("Your random cards are:");
    System.out.println("The " + CardNum + " of " + suitType);
    System.out.println("The " + CardNum1 + " of " + suitType1);
    System.out.println("The " + CardNum2 + " of " + suitType2);
    System.out.println("The " + CardNum3 + " of " + suitType3);
    System.out.println("The " + CardNum4 + " of " + suitType4);
    if (CardNum == CardNum1 || CardNum == CardNum2 || CardNum == CardNum3 || CardNum == CardNum4 || CardNum1 == CardNum2 || CardNum1 == CardNum3 || CardNum1 == CardNum4 || CardNum2 == CardNum3 || CardNum2 == CardNum4 || CardNum3 == CardNum4) { //finding pairs
      if (CardNum == CardNum1 && CardNum2 == CardNum3 || CardNum == CardNum1 && CardNum3 == CardNum4 || CardNum1== CardNum2 && CardNum == CardNum3 || CardNum1 == CardNum2 && CardNum == CardNum4 || CardNum1 == CardNum3 && CardNum2 == CardNum4 || CardNum1 == CardNum3 && CardNum == CardNum4 || CardNum1 == CardNum4 && CardNum == CardNum2 || CardNum1 == CardNum4 && CardNum == CardNum3 || CardNum1 == CardNum2 && CardNum3 == CardNum4 || CardNum1 == CardNum4 && CardNum2 == CardNum3 || CardNum2 == CardNum3 && CardNum == CardNum4 || CardNum2 == CardNum4 && CardNum == CardNum3 || CardNum3 == CardNum4 && CardNum == CardNum2) { //finding two pairs
        System.out.println("");
      System.out.println("You have two pairs!");
    }
      else {    
      System.out.println("");
      System.out.println("You have a pair!");}
    }
    if(CardNum == CardNum1 && CardNum == CardNum2 || CardNum == CardNum1 && CardNum == CardNum3 || CardNum == CardNum1 && CardNum == CardNum4 || CardNum == CardNum2 && CardNum == CardNum3 || CardNum == CardNum2 && CardNum == CardNum4 || CardNum == CardNum3 && CardNum == CardNum4 || CardNum1 == CardNum2 && CardNum1 == CardNum3 || CardNum1 == CardNum2 && CardNum1 == CardNum4 || CardNum1 == CardNum3 && CardNum1 == CardNum4 || CardNum2 == CardNum3 && CardNum2 == CardNum4) { //finding three of a kind outputs
      System.out.println("");
      System.out.println("You have a three of a kind!");
    }
    if (CardNum != CardNum1 && CardNum != CardNum2 && CardNum != CardNum3 && CardNum != CardNum4 && CardNum1 != CardNum2 && CardNum1 != CardNum3 && CardNum1 != CardNum4 && CardNum2 != CardNum3 && CardNum2 != CardNum4 && CardNum3 != CardNum4) {
      System.out.println("");
      System.out.println("You have a big card hand!");
    }
   
 }
}