///////////////////
// Ricky Hart Hw7
// this program will calculate the area of a triangle rectanggle and a circle

import java.util.Scanner; //importing scanner class

public class Area { 

      public static double circArea (double radius){
      double areaCirc = 3.14*radius*radius; //creates double for area
      System.out.println("The area of the circle is: " + areaCirc); //prints area
      return (areaCirc); //returns the desired value so we can exit
    }
   
  
      public static double rectArea (double height, double width){
        double areaRect = width*height; //creates double for area
        System.out.println("The area of the rectangle is: " + areaRect);//prints area
        return (areaRect);//returns the desired value so we can exit
      }


      public static double triArea (double height, double base){
        double areaTri = base*height*0.5;//creates double for area
        System.out.println("The area of the triangle is: " + areaTri);//prints area
        return(areaTri);//returns the desired value so we can exit
      }
  public static void main(String args[]){
  Scanner myScanner = new Scanner(System.in); //accepting scanner
  String junkWord; //initializes junkword
  boolean isString = false;//intializes string as false so it starts entering a number
  String shape = "";//intiales string
    do{
      System.out.print("Enter the name of a desired shape: "); //prints words to ask for shape
      if(myScanner.hasNext()){ //checks if value is a value
        shape = myScanner.next();//inserts into shape variable
          if(shape.equals("circle") || shape.equals("rectangle") || shape.equals("triangle")){ //checks if shape is equal to the strings we want
          isString = true;  //exits the loop
          }
      }
      else{
        System.out.println("Error. Make sure you enter a valid shape in lowercase: ");
        junkWord = myScanner.next(); //clears belt
      }
    } while(!(isString));  //keeps in loop until we get the desired result
  
  

    if (shape.equals("triangle")){ //triangle area
          System.out.print("Enter the value of the height: ");
          while (myScanner.hasNextDouble() == false){ //cheks is it is a usable number
            System.out.print("Error: Enter the value as a double. Try Again: ");
            junkWord = myScanner.next(); //clears belt
          }
        double height = myScanner.nextDouble(); //creates height
        System.out.print("Enter the value of the base: ");
          while (myScanner.hasNextDouble() == false){//cheks is it is a usable number
            System.out.print("Error: Enter the value as a double. Try Again: ");
            junkWord = myScanner.next(); //clears belt
          }
        double base = myScanner.nextDouble(); //creates base
      double area = triArea(height, base); //calls back to method
    }
    if (shape.equals("rectangle")){
      System.out.print("Enter the value of the height: ");
          while (myScanner.hasNextDouble() == false){//cheks is it is a usable number
            System.out.print("Error: Enter the value as a double. Try Again: ");
            junkWord = myScanner.next(); //clears belt
          }
        double height = myScanner.nextDouble(); //creates height
        System.out.print("Enter the value of the width: ");
          while (myScanner.hasNextDouble() == false){//cheks is it is a usable number
            System.out.print("Error: Enter the value as a double. Try Again: ");
            junkWord = myScanner.next(); //clears belt
          }
        double width = myScanner.nextDouble(); //creates width
      double area = rectArea(height, width); //calls back to method
    }
    if (shape.equals("circle")){
      System.out.print("Enter the value of the radius: ");
        while(myScanner.hasNextDouble() == false){//cheks is it is a usable number
          System.out.print("Error: Enter the value as a double. Try Again: ");
          junkWord = myScanner.next(); //clears belt
        }
      double radius = myScanner.nextDouble(); //creates radius
    double area = circArea(radius);//calls back to method
    }
  }
}