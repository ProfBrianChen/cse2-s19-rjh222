///////////////////
// Ricky Hart lab07
// this program will create a story of randomly generated components

import java.util.Random; //imports

public class Story2{
public static String action(String word2){ //action 
    String action = ""; //intializes
    Random randomGenerator = new Random();
    int s = randomGenerator.nextInt(2);  //generates random number
    switch(s){ //picks it or actual subject
      case 0: action = "It";
        break;
      case 1: action = "This " + word2;
        break;
    }
  return action; //returns
}
public static String adjective1(int x){
  String word1 = "";//intializes
  switch(x){ //gets word quick
    case 0: word1 = "quick";
            break;
  }
return word1;//returns
}
public static String adjective2(int b){
  String word5 = "";//intializes
  switch(b){ //gets word brown
    case 0: word5 = "brown";
            break;
  }
return word5;//returns
}
public static String adjective3(int c){
  String word6 = "";//intializes
  switch(c){ //gets word lazy
    case 0: word6 = "lazy";
            break;
  }
return word6;//returns
}
public static String subject(int y){
    String word2 = "";//intializes
  switch(y){ //gets word fox
    case 0: word2 = "fox";
            break;
  }
return word2;//returns
}
public static String verb(int z){
  String word3 = "";//intializes
  switch(z){ //gets word passed
    case 0: word3 = "passed";
            break;
  }
return word3;//returns
}
public static String object(int a){
  String word4 = "";//intializes
  switch(a){ //gets word dog
    case 0: word4 = "dog";
            break;
  }
return word4;//returns
  
}
public static String adverb(int d){
  String word7 = ""; //intializes
  switch(d){ //picks adv
     case 0: word7 = "extremely";
            break;
    case 1: word7 = "quite";
            break;
    case 2: word7 = "just";
            break;
    case 3: word7 = "almost";
            break;
    case 4: word7 = "very";
            break;
    case 5: word7 = "too";
            break;
    case 6: word7 = "enough";
            break;
    case 7: word7 = "abruptly";
            break;
    case 8: word7 = "lightly";
            break;
    case 9: word7 = "firmly";
            break;
  }
  return word7;//returns
}
public static String adjective4(int e){
  String word8 = ""; //intializes
  switch(e){ //picks adj
     case 0: word8 = "poor";
            break;
    case 1: word8 = "rich";
            break;
    case 2: word8 = "kind";
            break;
    case 3: word8 = "happy";
            break;
    case 4: word8 = "gentle";
            break;
    case 5: word8 = "polite";
            break;
    case 6: word8 = "proud";
            break;
    case 7: word8 = "wonderful";
            break;
    case 8: word8 = "zealous";
            break;
    case 9: word8 = "witty";
            break;
  }
  return word8;//returns
}
public static String adjective5(int f){
  String word9 = ""; //intializes
  switch(f){ //picks adj
     case 0: word9 = "passing";
            break;
    case 1: word9 = "flying";
            break;
    case 2: word9 = "wide";
            break;
    case 3: word9 = "skinny";
            break;
    case 4: word9 = "straight";
            break;
    case 5: word9 = "sleeping";
            break;
    case 6: word9 = "gigantic";
            break;
    case 7: word9 = "tiny";
            break;
    case 8: word9 = "scrawny";
            break;
    case 9: word9 = "miny";
            break;
  }
  return word9;//returns
}
public static String object1(int g){ //object
  String word10 = ""; //initializes
  switch(g){ //picks object
    case 0: word10 = "plane";
            break;
    case 1: word10 = "state";
            break;
    case 2: word10 = "church";
            break;
    case 3: word10 = "school";
            break;
    case 4: word10 = "day";
            break;
    case 5: word10 = "problem";
            break;
    case 6: word10 = "water";
            break;
    case 7: word10 = "fence";
            break;
    case 8: word10 = "yard";
            break;
    case 9: word10 = "house";
            break;
  }
return word10; //returns
}
public static void main(String args[]){
  int i = 1;//intializes
  Random randomGenerator = new Random(); //accepts
  int x = 0;//intializes
  int y = 0;//intializes
  int z = 0;//intializes
  int a = 0;//intializes
  int b = 0;//intializes
  int c = 0;//intializes
  String adj1 = adjective1(x); //calls method
  String adj2 = adjective2(b);//calls method
  String adj3 = adjective3(c);//calls method
  String obj = object(a);//calls method
  String sub = subject(y);//calls method
  String verb = verb(z);//calls method
  System.out.println("The " + adj1 + " " + adj2 + " " + sub + " " + verb + " the " + adj3 + " " + obj);
  int j = randomGenerator.nextInt(10);
  for(int t = 1; t<=j; t++){
  int d = randomGenerator.nextInt(10); //generates random number
  int e = randomGenerator.nextInt(10); //generates random number
  int f = randomGenerator.nextInt(10); //generates random number
  int g = randomGenerator.nextInt(10); //generates random number
  String act = action(sub);//calls method
  String adv = adverb(d);//calls method
  String adj4 = adjective4(e);//calls method
  String adj5 = adjective5(f);//calls method
  String obj1 = object1(a);//calls method
  System.out.println(act + " was " + adv + " " + adj4 + " to " + adj5 + " " + obj1);
}  
  String conc1 = action(sub);//calls method
  String conc2 = verb(z);//calls method
  String conc3 = object1(a);//calls method
  System.out.println(conc1 + " " + conc2 + " her " + conc3);


}  
}